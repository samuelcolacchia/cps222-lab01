# Makefile for CPS222 C++ usage lab
# There are three targets that can be made
# student (the default) compiles the student version of the program
# instructor compiles the instructor version of the program (which requires the
#  presence of the instructor files instructor_solutions.cc and instructor_solutions.h)
# distribution creates the distribution to be given to the students in folder c++lab and
#  the material for the instructor in folder lab_instructor

student: pet.cc pet.h
	g++ -o c++lab pet.cc date.o c++lab.o
	
instructor: pet.cc instructor_solutions.cc pet.h instructor_solutions.h date.cc date.h c++lab.cc 
	g++ -o c++lab -DINSTRUCTOR_VERSION pet.cc date.cc c++lab.cc
	
distribution: pet.cc pet.h date.cc date.h c++lab.cc
	echo 'Be sure this is being done on the architecture students will use in the lab'
	echo 'Be sure to print a copy of solutions.txt to put in the lab for student use'
	rm -rf ../c++lab
	mkdir ../c++lab
	g++ -c date.cc
	g++ -c -DINSTRUCTOR_VERSION c++lab.cc
	g++ -D INSTRUCTOR_VERSION pet.cc date.o c++lab.o
	./a.out > solutions.txt
	rm a.out
	cp pet.cc ../c++lab
	cp pet.h ../c++lab
	cp date.h ../c++lab
	mv date.o ../c++lab
	mv c++lab.o ../c++lab
	cp makefile ../c++lab	
	chmod -R a+r ../c++lab
	rm -rf ../lab_instructor
	mkdir ../lab_instructor
	cp c++lab.cc ../lab_instructor
	cp INSTRUCTOR_README.txt ../lab_instructor
	cp instructor_solutions.* ../lab_instructor
	mv solutions.txt ../lab_instructor
