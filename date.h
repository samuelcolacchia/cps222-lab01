/*
 * date.h - defines a representation for calendar dates
 *
 * Copyright (c) 1996, 1997, 2002, 2003, 2013 - Russell C. Bjork
 *
 */

// The following ensures this header is only included once
#ifndef DATE_H
#define DATE_H

#include <iostream>
#include <string>
using namespace std;

#pragma interface

class Date
{ 
    /*
     * PURPOSE: Represent a calendar date
     */

    public:

        /* Constructors: */

        // Default: initialize to today

        Date();

        // Initialize to a specific date. 

        // Argument must be of the form dd-mmm-yyyy - e.g. 01-JAN-1997
        Date(const string & representation);
        Date(const char * representation);
        // Date specified as a day in the range 1-31, month 1-12, 4 digit year
        Date(int day, int month, int year);

        /* Mutators */

        // These methods allow us to "fudge" the notion of today to facilitate
        // testing programs

        // today is set to specified value
        static void setToday(Date newValue);
        // today is set based on offset which is days +/- current value
        static void setToday(int offset); 
        
        // These methods allow us to alter a date by adding or subtracting an
        // offset expressed as a # of days
        
        Date & operator += (int offset);
        Date & operator -= (int offset);

        /* Accessors */

        // Access the current value of today
        
        static Date today();

        // These methods create a new date object by adding or subtracting an
        // offset expressed in # of days to/from an existing date

        Date operator + (int offset) const;
        Date operator - (int offset) const;
        
        // Calculate the difference (in days) between two dates
        
        int operator - (const Date & other) const;

        // Compare two dates, based on standard calendar order
        
        bool operator <  (const Date & other) const;
        bool operator <= (const Date & other) const;
        bool operator == (const Date & other) const;
        bool operator != (const Date & other) const;
        bool operator >= (const Date & other) const;
        bool operator >  (const Date & other) const;

        // Convert a date to a nicely-formatted string

        string toString() const;

        /* Input-output using i/o streams */

        friend istream & operator >> (istream & stream, Date & d);
        friend ostream & operator << (ostream & stream, const Date & d);

    private:

        long _dayNumber;
};

#endif
