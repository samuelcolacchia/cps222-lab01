/*
 * pet.cc - Implementation of class hierarchy declared in in pet.h
 * CPS222 Lab 1
 *
 * Copyright (c) 2001, 2002, 2003, 2013 - Russell C. Bjork
 *
 */

 /* TODO
 * 1. Ask questions
 * 2. Format clang for the semester
 * 3. Clean file
 * 4. Ask where to add comments, ie best practive .h or .cc
 */

#include <iostream>
#include <cmath>
using std::cout;
#include "pet.h"

// Basic rate information


//QUESTION purpose of these global like variables
//* Perhaps they are to be used like a global variable but rather than
// program wide they are file wide?
#define RODENT_DAILY_RATE	1
#define BIRD_DAILY_RATE		2
#define REPTILE_DAILY_RATE	3


Pet::Pet(string name, string owner, Date arrivalDate, Date departureDate)
	: _name(name), _owner(owner),
	_arrivalDate(arrivalDate), _departureDate(departureDate)
{
}


string Pet::getName() const
{
	return (_name);
}


void Pet::printBill() const
{
	cout << "To my owner, " << _owner << endl;
	cout	<< "I stayed at the Skunk Hollow Pet Hotel from " << _arrivalDate
		<< " to " << _departureDate << endl;
	cout	<< "I cost you: $" << getDailyRate() * (_departureDate - _arrivalDate)
		<< endl;
	cout << endl;
	cout << "Your loving " << getDescription() << ", " << _name << endl;
}


SmallAnimal::SmallAnimal(string name, string owner,
    Date arrivalDate, Date departureDate,
    string species)
	: Pet(name, owner, arrivalDate, departureDate), _species(species)
{
}


string SmallAnimal::getDescription() const
{
	return ("little " + _species);
}


Rodent::Rodent(string name, string owner, Date arrivalDate,
    Date departureDate, string species)
	: SmallAnimal(name, owner, arrivalDate, departureDate, species)
{
}


int Rodent::getDailyRate() const
{
	return (RODENT_DAILY_RATE);
}


Bird::Bird(string name, string owner, Date arrivalDate,
    Date departureDate, string species)
	: SmallAnimal(name, owner, arrivalDate, departureDate, species)
{
}


int Bird::getDailyRate() const
{
	return (BIRD_DAILY_RATE);
}


Reptile::Reptile(string name, string owner, Date arrivalDate, Date departureDate, string species)
//QUESTION Why is this needed
//* Filling in the information for our reptile?
	: SmallAnimal(name, owner, arrivalDate, departureDate, species)
{
}


int Reptile::getDailyRate() const
{
	return (REPTILE_DAILY_RATE);
}


MediumAnimal::MediumAnimal(string name, string owner, Date arrivalDate, Date departureDate, double weight, bool needsRabiesShot)
	: Pet(name, owner, arrivalDate, departureDate), _weight(weight), _needsRabiesShot(needsRabiesShot)
{
}


int MediumAnimal::getWeight() const
{
	return (_weight);
}


void MediumAnimal::printBill() const
{
	Pet::printBill();

	if (_needsRabiesShot)
	{
		cout << "P.S. I got a rabies shot. The vet will bill you later" << endl;
	}
	else
	{
		cout << "P.S. My rabies shot was up to date." << endl;
	}
}


Cat::Cat(string name, string owner, Date arrivalDate, Date departureDate, double weight, bool needsRabiesShot)
	: MediumAnimal(name, owner, arrivalDate, departureDate, weight, needsRabiesShot)
{
}


int Cat::getDailyRate() const
{
	if (getWeight() <= 15) {
		return (5);
	}else  {
		return (7);
	}
}

string Cat::getDescription() const
{
	if (getWeight() > 15)
	{
		return "Fat cat";
	}
	else
	{
		return "Cat";
	}
}

Dog::Dog(string name, string owner, Date arrivalDate, Date departureDate, double weight, bool needsRabiesShot, string breed)
: MediumAnimal(name, owner, arrivalDate, departureDate, weight, needsRabiesShot), _breed(breed)
{}

int Dog::getDailyRate() const
{
	if (getWeight() <= 20)
	{
		return 7;
	}
	else
	{
		double overWeight = getWeight() - 20;

		return ceil(overWeight/10) + 7;
	}
}

string Dog::getDescription() const
{
	return (_breed);
}

#ifdef INSTRUCTOR_VERSION
#include "instructor_solutions.cc"
#endif
