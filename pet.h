/*
 * pet.h - Class hierarchy for representing residents of a pet kennel
 * CPS222 Lab 1
 *
 * Copyright (c) 2001, 2002, 2013 - Russell C. Bjork
 *
 */

#include "date.h"

/* This class serves as a common base class for classes representing various
 * kinds of pets the kennel can house.
 */

class Pet
{
public:

	/* Constructor */
	Pet(string name, string owner, Date arrivalDate, Date departureDate);

	/* Accessor for name of this pet */
	string getName() const;

	/* Construct a string describing this pet */
	virtual string getDescription() const = 0;

	/* Accessor for daily rate for boarding this pet */
	virtual int getDailyRate() const = 0;

	/** Print a bill for boarding this pet */
	virtual void printBill() const;

private:

	// Instance variables
	string _name, _owner;
	Date _arrivalDate, _departureDate;
};

/* This class serves as a common base class for representing various kinds
 * of small, caged animals the kennel can house.
 */
class SmallAnimal : public Pet
{
public:

	/* Constructor */
	SmallAnimal(string name, string owner,
	    Date arrivalDate, Date departureDate,
	    string species);

	/* Construct a string describing this pet */
	string getDescription() const;

private:

	// Instance variable
	string _species;
};

/* This class represents rodents of various kinds.
 */
class Rodent : public SmallAnimal
{
public:

	Rodent(string name, string owner, Date arrivalDate,
	    Date departureDate, string species);

	int getDailyRate() const;
};

/* This class represents birds of various kinds.
 */
class Bird : public SmallAnimal
{
public:

	Bird(string name, string owner, Date arrivalDate, Date departureDate, string species);

	int getDailyRate() const;
};

class Reptile : public SmallAnimal
{
public:
	Reptile(string name, string owner, Date arrivalDate, Date departureDate, string species);

	int getDailyRate() const;
};

class MediumAnimal : public Pet
{
public:
	MediumAnimal(string name, string owner, Date arrivalDate, Date departureDate, double weight, bool needsRabiesShot);

	void printBill() const;

	int getWeight() const;

private:
	double _weight;
	bool _needsRabiesShot;
};

class Cat : public MediumAnimal
{
public:
	Cat(string name, string owner, Date arrivalDate, Date departureDate, double weight, bool needsRabiesShot);

	string getDescription() const;

	int getDailyRate() const;
};

class Dog : public MediumAnimal
{
public:
	Dog(string name, string owner, Date arrivalDate, Date departureDate, double weight, bool needsRabiesShot, string breed);

	string getDescription() const;

	int getDailyRate() const;
private:
	string _breed;
};



// REPLACE THE REST OF THIS FILE
// Students need to write declarations for classes Reptile, MediumAnimal,
// Cat and Dog.

#ifdef INSTRUCTOR_VERSION
#include "instructor_solutions.h"
#endif
